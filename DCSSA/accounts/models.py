from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Request(models.Model):
    time = models.DateTimeField()
    pnumber = models.IntegerField()
    bnumber = models.IntegerField()
    fnumber = models.CharField(max_length=10)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.IntegerField()  # -1: pending, 0: serving, 1: finished


class Match(models.Model):
    volunteer = models.ForeignKey(User, on_delete=models.CASCADE)
    req = models.OneToOneField(Request, on_delete=models.CASCADE)

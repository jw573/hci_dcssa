from django.shortcuts import render, redirect
from . import forms
from . import models
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.contrib.auth.forms import (
        PasswordChangeForm,
    )
from django.core.mail import send_mail
from datetime import date
from datetime import datetime
# Create your views here.


@login_required(login_url='/accounts/login/')
def log_out(request):
    logout(request)
    return redirect('/accounts/login')

def register(request):
    if request.method == 'POST':
        user_form = forms.UserCreationForm(request.POST)
        if user_form.is_valid():
            user_form.save()
            return HttpResponseRedirect(reverse('accounts:login'))
    else:
        user_form = forms.UserCreationForm()
    return render(request, '../templates/accounts/register.html', {'user_form': user_form})


@login_required(login_url='/accounts/login/')
def profile(request):
    user = request.user
    return render(request, 'accounts/profile.html', {'user': user})


@login_required(login_url='/accounts/login/')
def edit(request):
    user = request.user
    if request.method == 'POST':
        form = forms.UserForm(request.POST)
        user.email = request.POST['email']
        user.first_name = request.POST['first_name']
        user.last_name = request.POST['last_name']
        user.save()
        return render(request, 'accounts/edit_profile_done.html')
    else:
        form = forms.UserForm(instance=user)
    return render(request, 'accounts/edit.html', {'form': form, })


@login_required(login_url='/accounts/login/')
def req(request):
    user = request.user
    if request.method == 'POST':
        _time = request.POST['datetime1']
        _time = datetime.strptime(_time, "%m/%d/%Y %H:%M %p")
        time = datetime.strftime(_time, "%Y-%m-%d %H:%M")
        req = models.Request(owner_id=user.pk, time=time, pnumber=request.POST['Pnumber'],
                             bnumber=request.POST['Bnumber'], fnumber=request.POST['Fnumber'], status=-1)
        req.save()
        return HttpResponseRedirect(reverse('accounts:requestdone'))
    else:
        form = forms.RequestForm()
    return render(request, 'accounts/request.html', {'form': form, })


@login_required(login_url='/accounts/login/')
def req_done(request):
    return render(request, 'accounts/req_done.html')


@login_required(login_url='/accounts/login/')
def password_reset(request, uid):
    user = get_object_or_404(User, pk=uid)
    if user == request.user:
        if request.method == 'POST':
            form = PasswordChangeForm(data=request.POST, user=user)
            if form.is_valid():
                form.save()
                return redirect('/accounts/passwordresetdone')
        else:
            form = PasswordChangeForm(user=user)
            context = {
                'form': form,
            }
            return render(request, '../templates/accounts/password_reset.html', context)
    else:
        return render(request, '../templates/accounts/invalid_authentication.html')


@login_required(login_url='/accounts/login/')
def password_reset_done(request):
    return render(request, '../templates/accounts/password_reset_done.html')


@login_required(login_url='/accounts/login/')
def request_list(request, rid):
    user = request.user
    change = 0
    if rid != 65536:
        item = models.Request.objects.filter(pk=rid)[0]
        item.status = 1
        item.save()
        change = 1
    req = models.Request.objects.filter(owner_id=user.pk)
    return render(request, 'accounts/request_list.html', {'req': req, 'change': change})


@login_required(login_url='/accounts/login/')
def accept_list(request):
    user = request.user
    match = models.Match.objects.filter(volunteer_id=user.pk)
    return render(request, 'accounts/accept_list.html', {'acceptlist': match})

@login_required(login_url='/accounts/login/')
def search_req(request):
    if request.method == 'POST':
        form = forms.SearchForm(request.POST)
        _starttime = request.POST['datetime1']
        _endtime = request.POST['datetime2']
        _starttime = datetime.strptime(_starttime, "%m/%d/%Y %H:%M %p")
        starttime = datetime.strftime(_starttime, "%Y-%m-%d %H:%M")
        _endtime = datetime.strptime(_endtime, "%m/%d/%Y %H:%M %p")
        endtime = datetime.strftime(_endtime, "%Y-%m-%d %H:%M")
        req = models.Request.objects.filter(status=-1, time__range=(starttime, endtime))
    else:
        form = forms.SearchForm()
        req = models.Request.objects.filter(status=-1)
    return render(request, 'accounts/search_req.html', {'req': req, 'form': form})


@login_required(login_url='/accounts/login/')
def request_page(request, rid):
    req = get_object_or_404(models.Request, pk=rid)
    uid = request.user.pk
    if request.method == 'POST':
        if request.POST['type'] == "Accept":
            req.status = 0
            req.save()
            match = models.Match(volunteer_id=request.user.pk, req=req)
            match.save()
            print('send_email')
            msg = 'Hi '+ req.owner.first_name + ', Your request have been accepted by ' + request.user.first_name + \
                  ". He will contact you ASAP."
            send_mail(
                'DCSSA RSVP Notification',
                msg,
                'drew.ece590@gmail.com',
                [req.owner.email],
                fail_silently=False,
            )
            return render(request, 'accounts/accept_done.html')
        else:
            _time = request.POST['datetime1']
            _time = datetime.strptime(_time, "%m/%d/%Y %H:%M %p")
            time = datetime.strftime(_time, "%Y-%m-%d %H:%M")
            req.time = time
            req.pnumber = request.POST['Pnumber']
            req.bnumber = request.POST['Bnumber']
            req.fnumber = request.POST['Fnumber']
            req.save()
            return render(request, 'accounts/edit_request_done.html')
    else:
        form = forms.RequestForm(instance=req)
    context = {
        'req': req,
        'uid': uid,
        'form': form,
    }
    return render(request, 'accounts/request_page.html', context)

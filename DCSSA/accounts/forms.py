from django import forms
from datetimepicker.widgets import DateTimePicker
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from .models import Request


class UserCreationForm(UserCreationForm):

    class Meta:
        model = User
        # exclude = []
        fields = ("username", "email", "password1", "password2", "first_name", "last_name")


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("email", "first_name", "last_name")


class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'




class RequestForm(forms.ModelForm):
    class Meta:
        model = Request
        fields = ("time", "pnumber", "bnumber", "fnumber")
        widgets = {
            'time': DateTimeInput(),
        }


class SearchForm(forms.Form):
    start_time = forms.DateTimeField(widget=DateTimePicker(options={
        'format': '%Y-%m-%d %H:%M',
	    'language': 'en-us',
	}),)
    end_time = forms.DateTimeField(widget=DateTimePicker(options={
	    'format': '%Y-%m-%d %H:%M',
	    'language': 'en-us',
	}),)




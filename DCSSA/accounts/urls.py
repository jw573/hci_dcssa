from django.contrib.auth import views as auth_views
from django.urls import path, include
from . import views

app_name = 'accounts'
urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('register/', views.register, name='register'),
    path('profile/', views.profile, name='profile'),
    path('editprofile/', views.edit, name='edit'),
    path('request/', views.req, name='request'),
    path('requestdone/', views.req_done, name='requestdone'),
    path('passwordreset/<int:uid>', views.password_reset, name='passwordreset'),
    path('passwordresetdone/', views.password_reset_done, name='passwordresetdone'),
    path('requestlist/<int:rid>', views.request_list, name='requestlist'),
    path('acceptlist/', views.accept_list, name='accpetlist'),
    path('search/', views.search_req, name='search'),
    path('requestpage/<int:rid>', views.request_page, name='requestpage'),
    path('log_out/', views.log_out, name='log_out'),
]


